#include "../Block.h"

Block::Block()
{
	std::cout << "+ Block" << std::endl;
	height = 60;
	width = 100;
}
void Block::SetSize(Vector2u win, int BlocksCount)
{
	height = win.y / 23;
	width = ((win.x) / BlocksCount) - 5;
}
float Block::GetWidth()
{
	return width;
}
RectangleShape Block::Draw()
{
	RectangleShape Block(Vector2f(width, height));
	Block.setFillColor(Color::Blue);
	Block.setPosition(x, y);
	return Block;
}
Block::~Block()
{
	std::cout << "Удалился Block" << std::endl;
}