#include "../Player.h"
#include "Object.cpp"

Player::Player()
{
	std::cout << "+ Player" << std::endl;
}

void Player::SetPlayer(Vector2u win)//, float time)
{
	speedX = 0.001*win.x;// * time;
	width = win.x / 2;
	height = 10;
	x = win.x + (width/2);
	y = win.y - 20;
}

void Player::WallColision(Vector2u win)
{
	if (x <= 0)
	{
		x = 0;
	}
	if (Rx() >= win.x)
	{
		x = win.x - width;
	}
}
void Player::Control()
{
	if (Keyboard::isKeyPressed(Keyboard::Left))		// Движение ракетки
	{
		x -= speedX;	        					// Движение ракетки
	}
	if (Keyboard::isKeyPressed(Keyboard::Right))	// Движение ракетки
	{
		x += speedX;      							// Движение ракетки
	}
}
RectangleShape Player::Draw()
{
	RectangleShape Player(Vector2f(width, height));
	Player.setFillColor(Color::Red);
	Player.setPosition(x, y);
	return Player;
}
void Player::Update(Vector2u win)
{
	this->WallColision(win);
	this->Control();
}
Player::~Player()
{
	std::cout << "Удалился Player" << std::endl;
}