#include "../Ball.h"

Ball::Ball()
{
	std::cout << "+ Ball" << std::endl;
	height = 16;
	width = height;
}

void Ball::SetBall(Vector2u win)
{
	height = win.y / 50;
	width = height;
	x = win.x / 2;
	y = win.y / 2;
	speedX = 0.001 * win.x;
	speedY = 0.001 * win.y;
	dy = true;
	dx = true;
}

void Ball::Move()
{
	if (dy) y += speedY;
	else y -= speedY;

	if (dx) x += speedX;
	else x -= speedX;
}

void Ball::WallColision(Vector2u win)
{
	if ( x < 0 ) { x = 0; dx = !dx; }
	if ( y < 0 ) { y = 0; dy = !dy; }

	if ( x > win.x ) { x = win.x; dx = !dx; }

}

void Ball::PlayerColision(Player *player)
{
	bool itFirst = true;
	if( ( y + height > player->GetY() + 1 ) && ( this->Cx() > player->GetX()) && (this->Cx() < player->Rx() ) ) 
	{
		itFirst = false;
		speedX *= 2;
		dx = !dx;	
	}
	else if ((this->Cx() >= player->GetX()) && (this->Cx() <= player->Rx()) && y + height >= player->GetY() )
	{
		dy = !dy;
	}
	
}

bool Ball::BlockColision(Block *block)
{
	if( ( (y > block->GetY()) && (y+height < block->Dy()) && (x+width >= block->GetX()) && (x+width < block->Rx()) ) ) //Left
	{
		dy = !dy;
		return true;
	}
	if( (x+height/2 > block->GetX()) && (x+height/2 < block->Rx()) && (y < block->GetY()) && (y >= block->Dy()) )// Down
	{
		dx = !dx;
		return true;
	}
	if( (y > block->GetY()) && (y+height < block->Dy()) && (x > block->GetX()) && (x <= block->Rx()) )// Right
	{
		dy = !dy;
		return true;
	}
	if( (x+height/2 > block->GetX()) && (x+height/2 < block->Rx()) && (y+height > block->Dy()) && (y+height <= block->GetY() )) // Upper
	{
		dx = !dx;
		return true;
	}
	//return false;
}

float Ball::Cx()
{
	return x + width / 2;
}

float Ball::Cy()
{
	return y + height / 2;
}

void Ball::Update(Vector2u win)
{
	this->Move();
	//this->PlayerColision(win);
	this->WallColision(win);
}

CircleShape Ball::Draw()
{
	CircleShape ball(width / 2);
	ball.setPosition(x,y);
	ball.setFillColor(Color::Green);
	return ball;	
}

Vector2f Ball::GetSpeed()
{
	return Vector2f(speedX,speedY);
}

void Ball::SetSpeed(Vector2f a)
{
	speedX = a.x;
	speedY = a.y;
}
Ball::~Ball()
{
	std::cout << "Удалился Ball" << std::endl;
}
