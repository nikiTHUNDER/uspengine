#ifndef BALL_H
#define BALL_H 


#include "Object.h"

class Ball : public Object
{
private:
	bool 	dx, 
			dy;
	float	r; 		//радиус
	float 	speedX;	//
	float 	speedY;	//
public:
	Ball();
	void SetBall(Vector2u win);
	void Move();
	void WallColision(Vector2u win);
	void PlayerColision(Player *player);
	bool BlockColision(Block *block);
	float Cx();
	float Cy();
	void Update(Vector2u win);
	CircleShape Draw();
	Vector2f GetSpeed();
	void SetSpeed(Vector2f a);
	~Ball();
};

#endif