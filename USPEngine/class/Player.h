#ifndef PLAYER_H
#define PLAYER_H


#include "Object.h"

class Player : public Object
{
private:
	float 	speedX,
			speedY;
public:
	void SetPlayer(Vector2u win);//, float time);
	Player();
	void setSpeed();
	void WallColision(Vector2u win);
	void Control();
	void Update(Vector2u win);
	RectangleShape Draw();
	~Player();

};

#endif