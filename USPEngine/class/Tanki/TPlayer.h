#ifndef TPLAYER_H
#define TPLAYER_H 

#include "TEntity.h"
#include "TBullet.h"

class TPlayer : public TEntity
{
public:
	TPlayer();
	void Animation();
	void Update(Vector2f sk);
	void Control();
	void Move(int a);
	Sprite Shot();

	int ShotAnim();
	int GetShot();
	float GetReloadTime();

	bool CanShot();

	void Shoting();
	//vector<TBullet> GetBullet();
	//~TPlayer();
private:
	Clock animclock;
	Clock shotclock;
	int iFrame;
	int iShotFrame;
	Vector2f speed;
	int state;
	int shot;
	Image Ishot;
	Texture Tshot;
	Sprite Sshot;
	int isShot;

	bool canShoot;

	Clock reload;




	//vector<TBullet> bullet;
	//TBullet Buf;
};




#endif