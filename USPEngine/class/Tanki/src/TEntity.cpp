#include "../TEntity.h"




float TEntity::GetX()
{
	return x;
}

float TEntity::GetY()
{
	return y;
}

float TEntity::GetRX()
{
	return x + w;
}

float TEntity::GetDY()
{
	return y + h;
}

float TEntity::GetW()
{
	return w;
}

float TEntity::GetH()
{
	return h;
}

void TEntity::SetPosition(Vector2f a)
{
	x = a.x;
	y = a.y;
}

/*
void TEntity::SetImage(Image a)
{
	oimage = a;
}
*/

void TEntity::SetImage(string a)
{
	oimage.loadFromFile(a);
}

Sprite TEntity::DrawTexture()
{
	osprite.setPosition(x,y);
	return osprite;
}