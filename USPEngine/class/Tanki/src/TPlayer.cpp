#include "../TPlayer.h"
//#include "TBullet.cpp"


TPlayer::TPlayer()
{
	x = 100;
	y = 100;
	speed.x = 0.5;
	speed.y = 0.5;
	cout << "LOL" << endl;
	otexture.loadFromFile("USPEngine/res/Tanki/Tanks.png");
	cout << "LOL1" << endl;
	iFrame = 0;
	state = 0;
	Ishot.loadFromFile("USPEngine/res/Tanki/shots.png");
	Ishot.createMaskFromColor(Color(255, 255, 255));
	Tshot.loadFromImage(Ishot);
	iShotFrame = 0;
	shot = 0;
	canShoot = true;
}

void TPlayer::Animation()
{	
	//if (state == 1)
	//{
		float animtime = animclock.getElapsedTime().asSeconds();
		if (animtime >= 0.1) { iFrame++; animclock.restart(); }
		if (iFrame > 3) iFrame = 0;
		osprite.setTexture(otexture);
		osprite.setTextureRect(IntRect(0,iFrame*71,148,71));
		//osprite.setPosition(x,y+iFrame % 2);
	//}


	
	//osprite.setPosition(50, 25);
	//cout << "ANIMATON     " << iFrame << endl;
}

void TPlayer::Update(Vector2f sk)
{	
	this->Animation();
	state = 0;
	//cout << y << endl;
	if (reload.getElapsedTime().asSeconds() >= 1.5) canShoot = true;
	if (y + 71 > sk.y * 1080) y = -71 + sk.y * 1080;
	if (y < sk.y * 400) y = sk.y * 400;
	if (x + 148 > sk.x * 1920) x = -148 + sk.x * 1920;
	if (x < 0) x = 0;
	//for (int i = 0; i < bullet.size(); i++) bullet[i].Update();
	//cout << "UPDATE" << endl;
	this->Control();
}

void TPlayer::Move(int a)
{

}

Sprite TPlayer::Shot()
{
						  // 0   1   2   3   4   5    6   7    8    9    10    11    12    13    14
	float shot_pos_y[15] = { 0 , 0 , 0 , 6 , 3 , 0 , -2, -4 , -6 , -8 , -10 , -12 , -14 , -16 , -18 };
	float shottime = shotclock.getElapsedTime().asSeconds();
	if (shottime >= 0.07) { iShotFrame++; shotclock.restart(); }
	if (iShotFrame > 15) { shot = 0; iShotFrame = 0; }
	Sshot.setTexture(Tshot);
	Sshot.setTextureRect(IntRect(iShotFrame*70,0,70,50));
	if(state == 0) Sshot.setPosition(x+148-iShotFrame*4,y-10+shot_pos_y[iShotFrame]);
	if(state == 1) Sshot.setPosition(x-speed.x*iShotFrame,y);
	//cout << state << endl;
	return Sshot;
}

void TPlayer::Control()
{

	if (Keyboard::isKeyPressed(Keyboard::Up)) 		{ y-=speed.y; } //state = 1; }
	if (Keyboard::isKeyPressed(Keyboard::Down)) 	{ y+=speed.y; }//state = 1; }
	if (Keyboard::isKeyPressed(Keyboard::Left)) 	{ x-=speed.x; }//state = 1; }
	if (Keyboard::isKeyPressed(Keyboard::Right)) 	{ x+=speed.x; }//state = 1; }
	//
	
}

void TPlayer::Shoting()
{
	if (Keyboard::isKeyPressed(Keyboard::Space) && canShoot)
	{ 
		this->Shot(); 
		shot = 1; 
		canShoot = false;

        reload.restart(); 
	}
}

int TPlayer::ShotAnim()
{
	return shot;
}

int TPlayer::GetShot()
{
	return isShot;
}

float TPlayer::GetReloadTime()
{
	return reload.getElapsedTime().asSeconds();
}

bool TPlayer::CanShot()
{
	return canShoot;
}






/*
vector<TBullet> TPlayer::GetBullet()
{
	return bullet;
}*/