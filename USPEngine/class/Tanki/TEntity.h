#ifndef TENTITY_H
#define TENTITY_H

#include <string>

class TEntity
{
public:
	//TEntity();
	float GetX();
	float GetY();
	float GetRX();
	float GetDY();
	float GetW();
	float GetH();

	void SetPosition(Vector2f a);
	//void SetImage(Image a);
	void SetImage(string a);
	

	Sprite DrawTexture();


	//~TEntity();
protected:
	float		x,
				y,
				h,
				w;

	Image 		oimage;
	Texture 	otexture;
	Sprite 		osprite;
};






#endif