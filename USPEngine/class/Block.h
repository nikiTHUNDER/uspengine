#ifndef BLOCK_H
#define BLOCK_H

#include "Object.h"

class Block : public Object
{
public:
	Block();
	int GetBlocksCount();
	void SetSize(Vector2u win, int BlocksCount);
	float GetWidth();
	
	RectangleShape Draw();	
	~Block();
};

#endif