#ifndef OBJECT_H
#define OBJECT_H



class Object
{
protected:
	float 		x = 0,
				y = 0,
				width,
				height;
	bool 		isITcol;

    Image 		oimage;       
    Texture 	otexture; 
    Sprite 		osprite;
    
    
public:

	float GetX();
	float GetY();

	void SetPosition(float a, float b);
	float Rx();
	float Dy();
};

#endif