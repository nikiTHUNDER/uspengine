//////////////////////////////////////////////
//											//
//  ИГРОВОЙ ДВИЖОК  						//
//											//
//////////////////////////////////////////////
//											//
//  ВЕРСИЯ 0.0.9							//
//											//
//////////////////////////////////////////////

#ifndef SYSTEM_H
#define SYSTEM_H 

//////////////////////////////////////////////
//											//
//  SFML  БИБЛИОТЕКИ						//
//											//
//////////////////////////////////////////////
#include <SFML/Graphics.hpp>
using namespace sf;
//////////////////////////////////////////////
//  										//
//  XML										//
//											//
//////////////////////////////////////////////
#include "../Utils/PXML/src/pugixml.cpp"
using namespace pugi;
//////////////////////////////////////////////
//											//
//  СТАНДАРТНЫЙ БИБЛИОТЕКИ  				//
// 											//
//////////////////////////////////////////////
#include <iostream>
#include <vector>
#include <fstream>
#include <sstream>
#include <string>
using namespace std;
//////////////////////////////////////////////
//											//
//	ДОПОЛНИТЕЛЬНЫЕ МОДУЛИ					//
//											//
//////////////////////////////////////////////
//#include "StateManager.h"
//#include "Configuration.h"



struct Configuration
{
	Configuration();

	int iWindowWidth;
	int iWindowHeight;
	int iFrameLimit;

	float fTime;
	float fGameTick;
	float fGameSpeed;

	Vector2f resKoef;

	string sWindowTitle;
};





class StateBase;

class System
{
public:
	void Init();
	RenderWindow& GetWindow();
	Event& GetEvent();
	Configuration& GetConfiguration();

	void SetWindowTitle();
	void SetGameSpeed();
	
	//MANAGER
	void ChangeState(StateBase* state);
	void PushState(StateBase* state);
	void PopState();

private:

	sf::Event event;
	sf::RenderWindow window;
	sf::Clock clock;
	sf::Clock one_sec;

	int Frames;


	Configuration configuration;




	//////////////////////////////////////////
	//			 							//
	//  КОНФИГИ 							//
	//	(КОНФИГУРАЦИОННЫЕ ПЕРЕМЕННЫЕ)		//	 							
	//										//
	//////////////////////////////////////////
	
//-------------------------------------------------------------------------------------//
	void CreateWindow();
	void Render(RenderWindow& window);
	void MainLoop(StateBase& state);
	void Update(Event& event);


	vector<StateBase*> states;
	void Clock();
};
#endif