
#include "../System.h"
#include "../StateBase.h"
#include "../../states/StateIntro.h"
#include "SFML/Graphics.hpp"


void System::Init()
{	

	pugi::xml_document doc;
	pugi::xml_parse_result result = doc.load_file("USPEngine/system/system.xml");
	if (doc.load_file("USPEngine/system/system.xml"))
	{
	    configuration.iWindowWidth = doc.child("Settings").child("ScreenResolution").child("Width").text().as_int();
	    configuration.iWindowHeight = doc.child("Settings").child("ScreenResolution").child("Height").text().as_int();
	    configuration.sWindowTitle =  doc.child("Settings").child("Title").text().as_string();

	}
	else
	{
		cout << "XML PARSE ERROR" << endl;   
	}


	CreateWindow();
	this->ChangeState(StateIntro::Instance());
	MainLoop(*states.back());
	//-----------------------//
	//cout<<"Init sucsess"<<endl;
}

void System::CreateWindow()
{
	
	window.create(VideoMode(configuration.iWindowWidth,configuration.iWindowHeight), configuration.sWindowTitle);
	configuration.resKoef = Vector2f(window.getSize().x / 1920.f, window.getSize().y / 1080.f);
	cout << "WINDOW X: " << window.getSize().x << endl;
	cout << "WINDOW y: " << window.getSize().y << endl;
	cout << "KOEF X: " << configuration.resKoef.x << endl;
	cout << "KOEF Y: " << configuration.resKoef.y << endl;

	//-------------------------------//
	//cout << "CreateWindow sucsess" << endl;
}

void System::MainLoop(StateBase& state)
{
	
	while(window.isOpen())
	{

			Clock();
			Update(event);
		if (one_sec.getElapsedTime().asSeconds() < 1)
		{
			Render(window);
			Frames++;
		}
		else 
		{
			one_sec.restart();
			cout << "CURRENT FPS: " << Frames << endl;
			Frames = 0;
		}	

	}
    //------------------------//
	//cout << " end loop" << endl;
}

void System::Render(RenderWindow& window)
{
	window.clear();
	states.back()->Render(this);
	window.display();
	//-------------------------//
	//cout<<"Render sucsess"<<endl;
}

void System::Update(Event& event)
{
	while(window.pollEvent(event))
	{	
		states.back()->HandleEvents(this);

		if(event.type == Event::Closed)
		{
		 	window.close();
		}
	}
	states.back()->Update(this);
	//--------------------------//
	//cout<<"update loop end"<<endl;
}

RenderWindow& System::GetWindow()
{
	return window;
}

Event& System::GetEvent()
{
	return event;
}

Configuration& System::GetConfiguration()
{
	return configuration;
}




//////////////////////////
//						//
//  МАНАГЕР СТОСТОЯНИЙ  //
//						//
//////////////////////////
void System::ChangeState(StateBase* state)
{
	if ( !states.empty() ) {
		states.back()->Cleanup();
		states.pop_back();
	}
	states.push_back(state);
	states.back()->Init(this);
}

void System::PushState(StateBase* state){
	if ( !states.empty() ) {
		states.back()->Pause();
	}
	states.push_back(state);
	states.back()->Init(this);
}

void System::PopState()
{
	if ( !states.empty() ) {
		states.back()->Cleanup();
		states.pop_back();
	}
	if ( !states.empty() ) {
		states.back()->Resume();
	}
}


void System::Clock()
{
	configuration.fTime = clock.getElapsedTime().asMicroseconds();
	clock.restart();

	configuration.fTime /= configuration.fGameSpeed;

	if (configuration.fTime > configuration.fGameTick )
		configuration.fTime = configuration.fGameTick;

	//cout <<" ------------------     tick tock     --------------------------"<<endl;
	//cout << configuration.fTime << endl;
}


Configuration::Configuration()
{
	/////////////////////////////////
	//							   //
	//  КОНФИГУРАЦИИ ПО УМОЛЧАНИЮ  //
	//							   //
	/////////////////////////////////
	iWindowWidth = 800;//VideoMode::getDesktopMode().width;
	iWindowHeight = 600;//VideoMode::getDesktopMode().height;
	iFrameLimit = 60;

	fGameSpeed = 400.f;
	fGameTick = 40.f;
	fTime = 0.f;

	sWindowTitle = "";
};