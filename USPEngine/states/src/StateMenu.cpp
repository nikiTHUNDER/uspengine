#include "../StateMenu.h"
#include "StateArcanoid.cpp"
#include "StateTanki.cpp"
#include "StateCredits.cpp"

StateMenu StateMenu::m_StateMenu;

void StateMenu::Init(System* game)
{
	cout << "START INIT" << endl;

	Tbg.loadFromFile("USPEngine/res/img/TOP_MENU.png");
	Sbg.setTexture(Tbg);
	Sbg.scale(Vector2f(game->GetWindow().getSize().x/1920.f,game->GetWindow().getSize().y/1080.f));
	Sbg.setPosition(0,0);


	//printf("StateMenu Init\n");
	cout << "END INIT" << endl;
}

void StateMenu::Cleanup()
{
	
	//printf("StateMenu Cleanup\n");
}

void StateMenu::Pause()
{
	//printf("StateMenu Pause\n");
}

void StateMenu::Resume()
{
	clock.restart();
	//printf("StateMenu Resume\n");
}

void StateMenu::HandleEvents(System* game)
{
	//if (Keyboard::isKeyPressed(Keyboard::Space)) { game->PushState(StateArcanoid::Instance());}
	//if (Keyboard::isKeyPressed(Keyboard::Up)) { game->ChangeState(StateTanki::Instance());}


	if (Mouse::isButtonPressed(Mouse::Left))
	{
		Vector2f k = game->GetConfiguration().resKoef;
	 	if((mouse.y >= 235*k.y && mouse.y <= 603*k.y) && (mouse.x >= 344*k.x && mouse.x <= 845*k.x)) game->PushState(StateArcanoid::Instance());
	 	if((mouse.y >= 235*k.y && mouse.y <= 603*k.y) && (mouse.x >= 1068*k.x && mouse.x <= 1570*k.x)) game->PushState(StateTanki::Instance());
	 	if((mouse.y >= 1030*k.y && mouse.y <= 1055*k.y) && (mouse.x >= 1640*k.x && mouse.x <= 1900*k.x)) game->PushState(StateCredits::Instance());
	}
	if (Keyboard::isKeyPressed(Keyboard::Escape) && clock.getElapsedTime().asSeconds() > 0.2f) { game->GetWindow().close(); }





	//printf("StateMenu HandleEvents\n");
}

void StateMenu::Update(System* game) 
{
	mouse = game->GetWindow().mapPixelToCoords(Mouse::getPosition(game->GetWindow()));
}

void StateMenu::Render(System* game) 
{
	game->GetWindow().draw(Sbg);

	//printf("StateMenu Render\n");
}
