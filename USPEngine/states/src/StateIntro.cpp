#include "../StateIntro.h"
#include "StateMenu.cpp"

StateIntro StateIntro::m_StateIntro;

void StateIntro::Init(System* game)
{
	ImageBG.loadFromFile("USPEngine/res/img/kek.png");
	TextureBG.loadFromImage(ImageBG);
	SpriteBG.setTexture(TextureBG);
	Vector2u wsize = game->GetWindow().getSize();

	SpriteBG.setPosition( 0 + wsize.x / 2 - 250 , 0 );

	//printf("StateIntro Init\n");
}

void StateIntro::Cleanup()
{
	
	//printf("StateIntro Cleanup\n");
}

void StateIntro::Pause()
{
	//printf("StateIntro Pause\n");
}

void StateIntro::Resume()
{
	//printf("StateIntro Resume\n");
}

void StateIntro::HandleEvents(System* game)
{
	
	//printf("StateIntro HandleEvents\n");
}

void StateIntro::Update(System* game) 
{
	float time = clock.getElapsedTime().asSeconds();
	if (time >= 2) game->ChangeState(StateMenu::Instance());	
}

void StateIntro::Render(System* game) 
{
	
	game->GetWindow().draw(SpriteBG);
	//printf("StateIntro Render\n");
}
