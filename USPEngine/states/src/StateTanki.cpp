#include "../StateTanki.h"

//#include "../../class/Tanki/src/TPlayer.h"


StateTanki StateTanki::m_StateTanki;

void StateTanki::Init(System* game)
{
	SK = game->GetConfiguration().resKoef;
	wKoef = game->GetWindow().getSize().x/1920.f;
	hKoef = game->GetWindow().getSize().y/1080.f;
	Tbgsky.loadFromFile("USPEngine/res/Tanki/bgsky1.png");
	Sbgsky.setTexture(Tbgsky);
	Sbgsky.scale(SK);

	Tbgroad.loadFromFile("USPEngine/res/Tanki/bgroad1.png");
	Sbgroad.setTexture(Tbgroad);
	Sbgroad.scale(SK);
	Sbgroad.setPosition(0,445*SK.y);


	player.SetPosition(Vector2f(0,800*SK.y));

	iBgsky = 0;
	iBgroad = 0;
}

void StateTanki::Cleanup()
{
	Sbgsky.setScale(Vector2f(1.f,1.f));
	Sbgroad.setScale(Vector2f(1.f,1.f));
	printf("StateTanki Cleanup\n");
}

void StateTanki::Pause()
{
	printf("StateTanki Pause\n");
}

void StateTanki::Resume()
{
	printf("StateTanki Resume\n");
}

void StateTanki::HandleEvents(System* game)
{
	
	if(Keyboard::isKeyPressed(Keyboard::Space) && player.CanShot())
	{
		Buf.SetPosition(Vector2f(player.GetX()+150 , player.GetY()+10));
    	bullet.push_back(Buf);
	}
	player.Shoting();
	if(Keyboard::isKeyPressed(Keyboard::Escape)) { game->PopState(); }
	//printf("StateTanki HandleEvents\n");
}

void StateTanki::Update(System* game) 
{
	Vector2u winSize = game->GetWindow().getSize();
	player.Update(SK);
	for(int i = 0; i < bullet.size(); i++)
	{
		bullet[i].Update();
		if (bullet[i].GetX() > winSize.x) bullet.erase(bullet.begin()+i);	
	} 


	iBgsky += 0.25;
	if(iBgsky >= 1920) iBgsky = 0;
	Sbgsky.setTextureRect(IntRect(iBgsky,0,1920,1080));

	iBgroad += 0.5;
	if(iBgroad >= 1920) iBgroad = 0;
	Sbgroad.setTextureRect(IntRect(iBgroad,0,1920,1080));

	


	//for(int i = 0; i < player.GetBullet().size(); i++) player.GetBullet()[i].Update();
	//cout << "BULLETS = " << bullet.size() << endl;	
}

void StateTanki::Render(System* game) 
{
	game->GetWindow().draw(Sbgsky);
	game->GetWindow().draw(Sbgroad);
	game->GetWindow().draw(player.DrawTexture());
	if(player.ShotAnim()) game->GetWindow().draw(player.Shot());
	for(int i = 0; i < bullet.size(); i++) game->GetWindow().draw(bullet[i].DrawTexture());
	//printf("StateTanki Render\n");
}

void ScrollBackGround()
{
	
}