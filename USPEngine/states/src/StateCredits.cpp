#include "../StateCredits.h"

StateCredits StateCredits::m_StateCredits;

void StateCredits::Init(System* game)
{

}

void StateCredits::Cleanup()
{
	
	//printf("StateCredits Cleanup\n");
}

void StateCredits::Pause()
{
	//printf("StateCredits Pause\n");
}

void StateCredits::Resume()
{
	clock.restart();
	//printf("StateCredits Resume\n");
}

void StateCredits::HandleEvents(System* game)
{
	
	if(Keyboard::isKeyPressed(Keyboard::Escape)) { game->PopState(); }
	//printf("StateCredits HandleEvents\n");
}

void StateCredits::Update(System* game) 
{

}

void StateCredits::Render(System* game) 
{

	//printf("StateCredits Render\n");
}
