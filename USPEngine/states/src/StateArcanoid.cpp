#include "../StateArcanoid.h"
#include "../../class/Player.h"
#include "../../class/Block.h"

//#include "StateMenu.cpp"

#define BLOCKSCOUNT 15


auto LevelGenerator(vector<Block> blocks, Vector2u win)
{
	for (int i = 0; i < BLOCKSCOUNT; ++i)	
	{
		Block buf;
		buf.SetSize(win, BLOCKSCOUNT);
		buf.SetPosition(2.5f + i * (buf.GetWidth() + 5), 100);
		blocks.push_back(buf);
	}
	return blocks;
	
}
void GenMap(vector<Block> *blocks, Vector2u win)
{
	Block BLOCK_BUF;
	int shel = 20;

	for(int j = 0; j < 3; j++)
	{
		for(int i = 0; i < BLOCKSCOUNT - 1; i++)
		{
			BLOCK_BUF.SetPosition(i* (win.x / (BLOCKSCOUNT - 1)) + 1, shel);
			
			BLOCK_BUF.SetSize(win, BLOCKSCOUNT) ;
			blocks->push_back(BLOCK_BUF);
		}
		shel += 70;

		for(int i = 0; i < BLOCKSCOUNT - 1; i++)
		{
			if (i % 2 == 1)
			{
			BLOCK_BUF.SetSize(win, BLOCKSCOUNT) ;
			
			BLOCK_BUF.SetPosition(-1*(BLOCK_BUF.GetWidth() / 2) + i* (win.x / (BLOCKSCOUNT - 1)) + 1, shel) ;
			blocks->push_back(BLOCK_BUF);
			}
		}
		shel += 70;
	}
}





StateArcanoid StateArcanoid::m_StateArcanoid;

void StateArcanoid::Init(System* game)
{

	Vector2u winSize = game->GetWindow().getSize();
	player.SetPlayer(winSize);//, game->GetConfiguration().fTime);
	//printf("StateArcanoid Init\n");
	//blocks = LevelGenerator(blocks, winSize);
	GenMap(&blocks, winSize);
	ball.SetBall(winSize);
}

void StateArcanoid::Cleanup()
{
	//printf("StateArcanoid Cleanup - START\n");
	std::cout << blocks.size() << std::endl;
	blocks.clear();
	player.~Player();
	ball.~Ball();
	//printf("StateArcanoid Cleanup - END\n");
}

void StateArcanoid::Pause()
{	
	pausecd.restart();
	ballspeed = ball.GetSpeed();
	ball.SetSpeed(Vector2f(0,0));
	pause = true;
	//printf("StateArcanoid Pause\n");
}

void StateArcanoid::Resume()
{	
	pausecd.restart();
	ball.SetSpeed(ballspeed);
	pause = false;
	//printf("StateArcanoid Resume\n");
}

void StateArcanoid::HandleEvents(System* game)
{	
	if (Keyboard::isKeyPressed(Keyboard::Escape) && !pause && pausecd.getElapsedTime().asSeconds() >= 0.2) this->Pause();
	if (Keyboard::isKeyPressed(Keyboard::Escape) && pause && pausecd.getElapsedTime().asSeconds() >= 0.2) this->Resume();
	//printf("StateArcanoid HandleEvents\n");
}

void StateArcanoid::Update(System* game) 
{
	
	Vector2u winSize = game->GetWindow().getSize();
	player.Update(winSize);
	ball.Update(winSize);
	ball.PlayerColision(&player);
	for(int i = 0; i < blocks.size(); i++) if(ball.BlockColision(&blocks[i])) blocks.erase(blocks.begin()+i);
	if ((ball.GetY() > winSize.y) || (blocks.size() < 1)) game->PopState();
	
}

void StateArcanoid::Render(System* game) 
{
	
	game->GetWindow().draw(player.Draw());
	for (int i = 0; i < blocks.size(); ++i)
		game->GetWindow().draw(blocks[i].Draw());
	game->GetWindow().draw(ball.Draw());
	//printf("StateArcanoid Render\n");
}

