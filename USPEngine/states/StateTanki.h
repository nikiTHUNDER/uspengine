#ifndef STATETANKI_H
#define STATETANKI_H

#include "../system/StateBase.h"
#include "../class/Tanki/src/TPlayer.cpp"
#include "../class/Tanki/src/TEntity.cpp"
#include "../class/Tanki/src/TBullet.cpp"

class StateTanki : public StateBase
{
public:
	void Init(System* game);
	void Cleanup();

	void Pause();
	void Resume();

	void HandleEvents(System* game);
	void Update(System* game);
	void Render(System* game);

	static StateTanki* Instance() {
		return &m_StateTanki;
	}

protected:
	StateTanki() { }

private:
	static StateTanki m_StateTanki;


	float wKoef, hKoef;


	TPlayer player;
	vector<TBullet> bullet;
	TBullet Buf;

	Vector2f SK;

	//Image backgroundImage;
	//Texture backgroundTexture;
	//Sprite backgroundSprite;

	Texture Tbgroad;
	Texture Tbgsky;

	Sprite Sbgroad;
	Sprite Sbgsky;

	float iBgsky;
	float iBgroad;

};

#endif

