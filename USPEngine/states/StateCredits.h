#ifndef STATECREDITS_H
#define STATECREDITS_H

#include "../system/StateBase.h"

class StateCredits : public StateBase
{
public:
	void Init(System* game);
	void Cleanup();

	void Pause();
	void Resume();

	void HandleEvents(System* game);
	void Update(System* game);
	void Render(System* game);

	static StateCredits* Instance() {
		return &m_StateCredits;
	}

protected:
	StateCredits() { }

private:
	static StateCredits m_StateCredits;

	Clock clock;

};

#endif

