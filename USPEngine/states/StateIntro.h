#ifndef STATEINTRO_H
#define STATEINTRO_H

//#include "../system/System.h"
#include "../system/StateBase.h"

class StateIntro : public StateBase
{
public:
	void Init(System* game);
	void Cleanup();

	void Pause();
	void Resume();

	void HandleEvents(System* game);
	void Update(System* game);
	void Render(System* game);

	static StateIntro* Instance() {
		return &m_StateIntro;
	}

protected:
	StateIntro() { }

private:
	static StateIntro m_StateIntro;




	Clock clock;


	Image ImageBG;
	Texture TextureBG;
	Sprite SpriteBG;
};

#endif

