#ifndef STATEARCANOID_H
#define STATEARCANOID_H

#include "../system/StateBase.h"
#include "../class/src/Player.cpp"
#include "../class/src/Block.cpp"
#include "../class/src/Ball.cpp"


class StateArcanoid : public StateBase
{
public:
	void Init(System* game);
	void Cleanup();

	void Pause();
	void Resume();

	void HandleEvents(System* game);
	void Update(System* game);
	void Render(System* game);

	static StateArcanoid* Instance() {
		return &m_StateArcanoid;
	}

protected:
	StateArcanoid() { }

private:
	static StateArcanoid m_StateArcanoid;

	Player player;
	vector<Block> blocks;
	Ball ball;
	bool pause;

	Vector2f ballspeed;

	Clock pausecd;
};

#endif

