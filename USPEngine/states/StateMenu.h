#ifndef STATEMENU_H
#define STATEMENU_H

//#include "../system/System.h"
#include "../system/StateBase.h"

class StateMenu : public StateBase
{
public:
	void Init(System* game);
	void Cleanup();

	void Pause();
	void Resume();

	void HandleEvents(System* game);
	void Update(System* game);
	void Render(System* game);

	static StateMenu* Instance() {
		return &m_StateMenu;
	}

protected:
	StateMenu() { }

private:
	static StateMenu m_StateMenu;

	Clock clock;
	

	Vector2f mouse;

	Texture Tbg;
	Texture TArc;
	Texture TTnk;

	Sprite Sbg;
	Sprite SArc;
	Sprite STnk;

	Image ImageBG;
	Texture TextureBG;
	Sprite SpriteBG;
};

#endif

